Your project final project should include the following files and folders:

The .circleci/config.yml file

A root level package.json

A README with project overview, screenshot of the working application, and a link to your hosted working frontend application.

Screenshots for the following:

Last successful CircleCi build. You can provide screenshots for each phase - build, hold, and deploy separately.
AWS RDS for the database overview
AWS ElasticBeanstalk for the (backend) API deployment
AWS S3 for (frontend) web hosting
A docs folder to include:

An architecture diagram showing a high-level overview of the infrastructure
A diagram showing the overview of the pipeline
Infrastructure_description.md - Document the infrastructure needs (RDS, S3 Elastic Beanstalk, etc). The diagram Includes the different AWS services used for hosting the API, Frontend, and the DB.``
Pipeline_description.md - Explain the different steps in the pipeline
Application_dependencies.md
Suggestions to Make Your Project Stand Out!
Make your pipeline run the front-end unit tests! You will need to figure out how to run mocha in a CI env
Make Pull Requests builds so that each time you open a pull request against the main branch, your tests and build are executed.
Make separate environments for staging and production in elastic beanstalk and s3. Deploy PR code to the staging and Main to Production.
Ensure look at the project rubric and satisfy each and every criteria before making a submission.

